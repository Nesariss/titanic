import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    # Extract titles from the 'Name' column with a dot at the end
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+\.)')

    # Define titles to consider
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]

    missing_counts2 = {}  
    

    for title in titles_to_consider:
        missing_count = df[df['Name'].str.contains(title)]['Age'].isna().sum()
        missing_counts2[title] = missing_count

    # Calculate medians for the specified titles
    median_age_by_title = df[df['Title'].isin(titles_to_consider)].groupby('Title')['Age'].median().to_dict()

    # Fill missing 'Age' values based on titles
    for title in titles_to_consider:
        df.loc[(df['Title'] == title) & (df['Age'].isnull()), 'Age'] = median_age_by_title[title]


    # Create the final list of tuples
    result = [(title, missing_counts2[title], int(median_age_by_title[title])) for title in titles_to_consider]
    return result